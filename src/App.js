import './index.css';


import { ReactComponent as ArrowIcon } from './icons/arrow.svg'
import { ReactComponent as BellIcon } from './icons/bell.svg'
import { ReactComponent as BoltIcon } from './icons/bolt.svg'
import { ReactComponent as CaretIcon } from './icons/caret.svg'
import { ReactComponent as ChevronIcon } from './icons/chevron.svg'
import { ReactComponent as CogIcon } from './icons/cog.svg'
import { ReactComponent as MessengerIcon } from './icons/messenger.svg'
// import { ReactComponent as PlusIcon } from './icons/plus.svg'

import React, { useState } from 'react'
import { CSSTransition } from 'react-transition-group'

function App () {
  return (
    <NavBar>
      <NavItem icon={<BoltIcon />} />
      <NavItem icon={<BellIcon />} />
      <NavItem icon={<MessengerIcon />} />

      <NavItem icon={<CaretIcon />}>
        <DropdownMenu />
      </NavItem>
    </NavBar>
  )
}

function NavBar (props) {
  return (
    <nav className='navbar'>
      <ul className='navbar-nav'>
        {props.children}
      </ul>
    </nav>
  )
}

function NavItem (props) {
  const [open, setOpen] = useState(false)

  return (
    <li className='nav-item'>
      <a href='#' className='icon-button' onClick={() => setOpen(!open)}>
        {props.icon}
      </a>

      {open && props.children}

    </li>
  )
}

function DropdownMenu () {
  const [activeMenu, setActiveMenu] = useState('main')
  const [menuHeight, setMenuHeight] = useState(null)

  function calcHeight(el) {
    const height = el.offsetHeight
    setMenuHeight(height)
  }

  function DropdownItem (props) {
    return (
      <a
        href='#'
        className='menu-item'
        onClick={() => props.goTomenu && setActiveMenu(props.goTomenu)}
      >
        <span className='icon-button'>{props.leftIcon}</span>
        {props.children}
        <span className='icon-right'>{props.rightIcon}</span>
      </a>
    )
  }

  return (
    <div className='dropdown' style={{ height: menuHeight}}>

      <CSSTransition
        in={activeMenu === 'main'}
        classNames='menu-primary'
        unmountOnExit
        timeout={500}
        onEnter={calcHeight}
      >
        <div className='menu'>
          <DropdownItem>My Profile</DropdownItem>
          <DropdownItem
            leftIcon={<CogIcon />}
            rightIcon={<ChevronIcon />}
            goTomenu='settings'
          >
            Settings
          </DropdownItem>
          <DropdownItem
            leftIcon={<CogIcon />}
            rightIcon={<ChevronIcon />}
            goTomenu='animals'
          >
            Animals
          </DropdownItem>
        </div>
      </CSSTransition>

      <CSSTransition
        in={activeMenu === 'settings'}
        classNames='menu-secondary'
        unmountOnExit
        timeout={500}
        onEnter={calcHeight}
      >
        <div className='menu'>
          <DropdownItem goTomenu='main' leftIcon={<ArrowIcon />}>
            <h4>Main menu</h4>
          </DropdownItem>
          <DropdownItem leftIcon={<BoltIcon />}>Setting</DropdownItem>
          <DropdownItem leftIcon={<BoltIcon />}>Setting</DropdownItem>
          <DropdownItem leftIcon={<BoltIcon />}>Setting</DropdownItem>
          <DropdownItem leftIcon={<BoltIcon />}>Setting</DropdownItem>
        </div>
      </CSSTransition>

      <CSSTransition
        in={activeMenu === 'animals'}
        unmountOnExit
        timeout={500}
        classNames='menu-secondary'
        onEnter={calcHeight}
      >
        <div className='menu'>
          <DropdownItem goTomenu='main' leftIcon={<ArrowIcon />}>
            <h4>Animals</h4>
          </DropdownItem>
          <DropdownItem leftIcon={<BoltIcon />}>Animal</DropdownItem>
          <DropdownItem leftIcon={<BoltIcon />}>Animal</DropdownItem>
          <DropdownItem leftIcon={<BoltIcon />}>Animal</DropdownItem>
          <DropdownItem leftIcon={<BoltIcon />}>Animal</DropdownItem>
        </div>
      </CSSTransition>
    </div>
  )
}

export default App
